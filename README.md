# dotfiles

![screenshot](https://gitlab.com/fbarn/dotfiles/raw/master/screenshot.png)

## My custom CWM config files.

These include:
- Vim-like settings for CWM 
- Additional xdotool/wmutils-based commands:
    - cw: center window and mouse.
    - mmv: move mouse up, right, left, or down.
    - wfit: resize window such at it fits in given space.
    - wrev: mirror the currently visible windows either horizontally or vertically.
    - wst: stack all windows in the active group such that they take up 100% of vertical space and 50% of horizontal space. 
- Asynchronous, signal-based, and easily configurable [termbar](https://github.com/vetelko/termbar) which shows:
    - Real-time group activity 
        - ❖: current active group; 
        - ⬥: all other visible groups; 
        - ⟡: all other hidden groups (with windows); 
        - ⬦: groups with no windows
    - Numer of terminals, SSH's, and instances of neovim currently running
    - Memory usage, battery, brightness, volume, date, time, wifi connection

Some of my code was taken from the following sources:
[LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice)
[wmutils](https://github.com/arcetera/wmrc)
